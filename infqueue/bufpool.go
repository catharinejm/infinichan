package infqueue

import (
	"math/bits"
	"runtime"
	"sync/atomic"
	"unsafe"
)

type BufPool interface {
	getBuffer() *buffer
}

type bufpool struct {
	buffers *poolNode
}

var claimedNode poolNode = poolNode{}

func newBufpool() *bufpool {
	return &bufpool{
		buffers: &poolNode{},
	}
}

func (p *bufpool) getBuffer() *buffer {
	node := p.head()
	for {
		mask := node.mask()
		idx := bits.TrailingZeros32(^mask)
		if idx == 32 {
			if next := node.next(); next != nil {
				node = next
			}
			continue
		}

		if node.claim(idx) {
			buf := node.bufs[idx]
			if buf == nil {
				buf = &buffer{}
			}
			node.bufs[idx] = nil
			runtime.SetFinalizer(buf, bufferFinalizer(node, idx))
			return buf
		}
	}
}

func bufferFinalizer(node *poolNode, idx int) func(*buffer) {
	return func(buf *buffer) {
		*buf = buffer{}
		node.bufs[idx] = buf
		node.unclaim(idx)
	}
}

func (p *bufpool) head() *poolNode {
	var headP *unsafe.Pointer = (*unsafe.Pointer)(unsafe.Pointer(&p.buffers))
	return (*poolNode)(atomic.LoadPointer(headP))
}

type poolNode struct {
	_mask uint32
	bufs  [32]*buffer
	_next *poolNode
}

func (n *poolNode) mask() uint32 {
	return atomic.LoadUint32(&n._mask)
}

func (n *poolNode) claim(idx int) bool {
	for {
		mask := n.mask()
		newMask := mask | (1 << uint(idx))
		if mask == newMask {
			return false
		}

		if atomic.CompareAndSwapUint32(&n._mask, mask, newMask) {
			return true
		}
	}
}

func (n *poolNode) unclaim(idx int) {
	for {
		mask := n.mask()
		if atomic.CompareAndSwapUint32(&n._mask, mask, mask&^(1<<uint(idx))) {
			return
		}
	}
}

func (n *poolNode) next() *poolNode {
	var nextP *unsafe.Pointer = (*unsafe.Pointer)(unsafe.Pointer(&n._next))
	switch n := (*poolNode)(atomic.LoadPointer(nextP)); n {
	case nil:
		if atomic.CompareAndSwapPointer(nextP, nil, unsafe.Pointer(&claimedNode)) {
			next := &poolNode{}
			atomic.StorePointer(nextP, unsafe.Pointer(next))
			return next
		}
		return nil
	case &claimedNode:
		return nil
	default:
		return n
	}
}
