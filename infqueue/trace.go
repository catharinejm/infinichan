package infqueue

type TraceInfo struct {
	RetryCount       int
	FailIncHighCount int
	AddTail          int
	Aborted          int
}

func (t *TraceInfo) Add(other *TraceInfo) {
	t.RetryCount += other.RetryCount
	t.FailIncHighCount += other.FailIncHighCount
	t.AddTail += other.AddTail
	t.Aborted += other.Aborted
}
