package infqueue

import (
	"context"
	"fmt"
	"math/bits"
	"runtime"
	"sync/atomic"
	"unsafe"

	"gitlab.com/catharinejm/infinichan/config"
	"gitlab.com/catharinejm/infinichan/utils"
)

const BUFFER_SIZE = 32

var _SPINCAP int

func init() {
	_SPINCAP = config.Base().SpinCap
}

func SPINCAP() int {
	return _SPINCAP
}

type Queue interface {
	fmt.GoStringer
	Push(x interface{})
	Shift() interface{}
}

type QueueTrace interface {
	Queue
	PushTrace(ctx context.Context, x interface{}) *TraceInfo
}

type dummyBufPool struct{}

func (dummyBufPool) getBuffer() *buffer {
	return &buffer{}
}

func NewQueue() Queue {
	// pool := newBufpool()
	pool := dummyBufPool{}
	buf := pool.getBuffer()
	return &queue{
		_head: buf,
		_tail: buf,
		pool:  pool,
	}
}

type queue struct {
	_head *buffer
	_tail *buffer
	pool  BufPool
}

func (q *queue) Push(x interface{}) {
	attempts := 0
	for q.push(x)&pushSuccess == 0 {
		attempts++
		if attempts == _SPINCAP {
			runtime.Gosched()
			attempts = 0
		}
	}
}

func (q *queue) PushTrace(ctx context.Context, x interface{}) *TraceInfo {
	traceInfo := &TraceInfo{}
	attempts := 0
	for {
		attempts++
		if attempts == _SPINCAP {
			runtime.Gosched()
			attempts = 0
		}
		select {
		case <-ctx.Done():
			traceInfo.Aborted = 1
			return traceInfo
		default:
			result := q.push(x)
			if result&pushSuccess != 0 {
				traceInfo.AddTail += bits.OnesCount(uint(result & pushAddTail))
				return traceInfo
			}
			traceInfo.FailIncHighCount += bits.OnesCount(uint(result & pushIncTail))
			traceInfo.RetryCount++
		}
	}
}

type pushResult uint

const (
	pushRetry   = 0
	pushSuccess = 1 << iota
	pushAddTail
	pushIncTail
)

func (q *queue) push(x interface{}) (result pushResult) {
	tail := q.tail()
	if tail.high() >= BUFFER_SIZE {
		return
	}
	idx := tail.incHigh()
	result |= pushIncTail

	if idx < BUFFER_SIZE {
		node := &tail.contents[idx]
		node.value = x
		atomic.StoreInt32(&node.filled, 1)

		if idx+1 == BUFFER_SIZE {
			result |= pushAddTail

			newTail := q.pool.getBuffer()
			storeBuffer(&tail._next, newTail)
			storeBuffer(&q._tail, newTail)
		}

		result |= pushSuccess
		return
	}
	return
}

func (q *queue) Shift() interface{} {
	for {
		head := q.head()
		for {
			low := head.low()
			if low >= utils.MinInt(head.high(), BUFFER_SIZE) {
				break
			}

			if head.tryIncLow(low) {
				node := &head.contents[low]
				for {
					if atomic.LoadInt32(&node.filled) == 1 {
						break
					}
				}
				if low+1 == BUFFER_SIZE {
					for {
						next := head.next()
						if next != nil {
							storeBuffer(&q._head, next)
							break
						}
					}
				}
				result := node.value
				node.value = nil
				return result
			}
		}

		if head.next() == nil {
			return nil
		}
	}
}

func (q *queue) head() *buffer {
	return loadBuffer(&q._head)
}

func (q *queue) tail() *buffer {
	return loadBuffer(&q._tail)
}

func loadBuffer(bufP **buffer) *buffer {
	var unsafeBufP *unsafe.Pointer = (*unsafe.Pointer)(unsafe.Pointer(bufP))
	return (*buffer)(atomic.LoadPointer(unsafeBufP))
}

func storeBuffer(bufP **buffer, buf *buffer) {
	var unsafeBufP *unsafe.Pointer = (*unsafe.Pointer)(unsafe.Pointer(bufP))
	atomic.StorePointer(unsafeBufP, unsafe.Pointer(buf))
}

/// impl fmt.GoStringer for *queue
func (q *queue) GoString() string {
	return fmt.Sprintf("Queue{head:%#v, tail:%#v}", q.head(), q.tail())
}

type buffer struct {
	_high    int32
	_low     int32
	contents [BUFFER_SIZE]bufElem
	_next    *buffer
}

type bufElem struct {
	value  interface{}
	filled int32
}

func (b *buffer) high() int {
	return int(atomic.LoadInt32(&b._high))
}

func (b *buffer) incHigh() int {
	return int(atomic.AddInt32(&b._high, 1) - 1)
}

func (b *buffer) low() int {
	return int(atomic.LoadInt32(&b._low))
}

func (b *buffer) tryIncLow(low int) bool {
	return atomic.CompareAndSwapInt32(&b._low, int32(low), int32(low+1))
}

func (b *buffer) next() *buffer {
	return loadBuffer(&b._next)
}
