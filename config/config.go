package config

import "flag"

var (
	cpuprofile  = flag.String("cpuprofile", "", "write cpu profile to file")
	elemCount   = flag.Int("count", 10000, "number of elements to push/pop")
	threadCount = flag.Int("threads", 100, "number of threads")
	runTrace    = flag.Bool("trace", false, "trace queue usage")
	spinCap     = flag.Int("spincap", 100, "number of spins before yielding the goroutine")

	base Config
)

type Config struct {
	CPUProfile  string
	ElemCount   int
	ThreadCount int
	RunTrace    bool
	SpinCap     int
}

func init() {
	flag.Parse()
	base = Config{
		CPUProfile:  *cpuprofile,
		ElemCount:   *elemCount,
		ThreadCount: *threadCount,
		RunTrace:    *runTrace,
		SpinCap:     *spinCap,
	}
}

func Base() Config {
	return base
}
